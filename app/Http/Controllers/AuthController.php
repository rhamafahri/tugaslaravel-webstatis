<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regist(){
        return view('page.formRegist');
    }

    public function greet(Request $ambil) {
        $namaAwal = $ambil['Nama_Awal'];
        $namaAkhir = $ambil['Nama_Akhir'];

        return view('page.pageWelcome', compact('namaAwal', 'namaAkhir'));
    }
}
