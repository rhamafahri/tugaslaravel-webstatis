<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@home');

Route::get('/register', 'AuthController@regist');

Route::post('/welcome', 'AuthController@greet');

Route::get('/master', function(){
    return view('layout.master');
});

Route::get('/data-table', function(){
    return view('table.data-table');
});

// --- CRUD Cast ---
// Create 
Route::get('/cast/create', 'CastController@create'); //Route menuju form create
Route::post('/cast', 'CastController@store'); // Route untuk menyimpan form ke database

// Read
Route::get('/cast', 'CastController@index'); // Route untuk read data
Route::get('/cast/{cast_id}', 'CastController@show'); //Menampilkan detail

//Update
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); // Route Untuk edit Data
Route::put('/cast/{cast_id}', 'CastController@update'); //Route untuk update data

///Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy'); // Route untuk menghapus data