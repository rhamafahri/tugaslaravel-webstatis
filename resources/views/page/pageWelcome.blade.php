@extends('layout.master')

@section('judul')
Halaman Index
@endsection

@section('content')
<h1>SELAMAT DATANG! {{$namaAwal}} {{$namaAkhir}}</h1>
<h3>Terimakasih telah bergabung di website kami, media belajar kita bersama!</h3>
@endsection