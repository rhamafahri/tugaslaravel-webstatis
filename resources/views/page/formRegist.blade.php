@extends('layout.master')

@section('judul')
Buat account baru!
@endsection

@section('content')
<h3>Sign Up Form</h3><br>
  <form action="/welcome" method="post">
    @csrf 
    <label for="">First name: </label><br>
    <input type="text" name="Nama_Awal"><br><br>
    <label for="">Last name: </label><br>
    <input type="text" name="Nama_Akhir"><br><br>
    <label for="">Gender</label><br>
    <input type="radio" name="Jenis_Kelamin" value="Laki"><label for="laki">Male</label><br>
    <input type="radio" name="Jenis_Kelamin" value="Wanita"><label for="">Female</label><br>
    <input type="radio" name="Jenis_Kelamin" value="Other"><label for="">Other</label><br><br>
    <label for="">Nationality</label><br>
    <select id="" name="Warga_Negara">
      <option value="Indonesia"  >Indonesia</option>
      <option value="WNA" >WNA</option>
    </select><br><br>
    <label for="">Language Spoken</label><br>
    <input type="checkbox" name="Bahasa1" value="Indonesia"><label for="">Indonesia</label><br>
    <input type="checkbox" name="Bahasa2" value="English"><label for="">English</label><br>
    <input type="checkbox" name="Bahasa3" value="Other"><label for="">Other</label><br><br>
    <label for="">Bio</label><br>
    <textarea name="" id="" cols="30" rows="10"></textarea><br><br>
    <input type="submit" value="Sign Up">
  </form>
@endsection