@extends('layout.master')

@section('judul')
Media Online
@endsection

@section('content')
<h1>Media Online</h1>
        <h3>Social Media Developer</h3>
        <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
        <h3>Benefit join di media online</h3>
        <ul>
            <li>Mendapat motivasi sesama developer</li>
            <li>Sharing knowledge</li>
            <li>Dibuat oleh calon web developer terbaik</li>
        </ul>
        <h3>Cara bergabung ke media online</h3>
        <ol>
            <li>Mengunjungi website ini</li>
            <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
            <li>Selesai</li>
        </ol>
@endsection